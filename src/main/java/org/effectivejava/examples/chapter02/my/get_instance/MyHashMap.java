package org.effectivejava.examples.chapter02.my.get_instance;

import java.util.HashMap;

/**
 * @author Alterovych Ilya
 */
public class MyHashMap extends HashMap {
    public static <K, V> HashMap<K, V> newInstance() {
        return new HashMap<K, V>();
    }
}
