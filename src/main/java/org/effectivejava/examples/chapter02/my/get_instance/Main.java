package org.effectivejava.examples.chapter02.my.get_instance;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author Alterovych Ilya
 */
public class Main {
    public static void main(String[] args) {
        Map<String, List<String>> map = MyHashMap.newInstance();

        List<String> list = new ArrayList<>();
        list.add("firs");
        list.add("second");
        list.add("third");

        map.put("1", list);
        System.out.println(Arrays.toString(map.get("1").toArray()));
    }
}
